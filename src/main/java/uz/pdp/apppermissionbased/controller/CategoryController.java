package uz.pdp.apppermissionbased.controller;

import jakarta.validation.Valid;
import lombok.Getter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apppermissionbased.entity.Category;
import uz.pdp.apppermissionbased.payload.CategoryDTO;

import java.util.List;
import java.util.UUID;

@RequestMapping(CategoryController.BASE_PATH)
public interface CategoryController {
    String BASE_PATH = "/api/category";

    @PreAuthorize("hasAuthority('CATEGORY_ADD')")
    @PostMapping
    String add(@RequestBody @Valid CategoryDTO categoryDTO);

    @PreAuthorize("hasAuthority('CATEGORY_EDIT')")
    @PutMapping("/{id}")
    String edit(@RequestBody @Valid CategoryDTO categoryDTO, @PathVariable UUID id);

    @PreAuthorize("hasAuthority('CATEGORY_DELETE')")
    @DeleteMapping("/{id}")
    String delete(@PathVariable UUID id);

    @PreAuthorize("hasAuthority('CATEGORY_LIST')")
    @GetMapping
    List<Category> list();

    @PreAuthorize("hasAuthority('CATEGORY_BY_ID')")
    @GetMapping("/{id}")
    Category categoryById(@PathVariable UUID id);
}
