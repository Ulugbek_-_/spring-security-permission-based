package uz.pdp.apppermissionbased.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apppermissionbased.entity.Category;
import uz.pdp.apppermissionbased.payload.CategoryDTO;
import uz.pdp.apppermissionbased.service.CategoryService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CategoryControllerImpl implements CategoryController {
    private final CategoryService categoryService;
    @Override
    public String add(CategoryDTO categoryDTO) {
        String message = categoryService.addCategory(categoryDTO);
        return message;
    }

    @Override
    public String edit(CategoryDTO categoryDTO, UUID id) {
        String message = categoryService.editCategory(categoryDTO, id);
        return message;
    }

    @Override
    public String delete(UUID id) {
        return categoryService.deleteCategory(id);
    }

    @Override
    public List<Category> list() {
        List<Category> categories = categoryService.categoryList();
        return categories;
    }

    @Override
    public Category categoryById(UUID id) {
        Category category = categoryService.getCategory(id);
        return category;
    }
}
