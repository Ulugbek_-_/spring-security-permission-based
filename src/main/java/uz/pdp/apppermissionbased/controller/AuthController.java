package uz.pdp.apppermissionbased.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apppermissionbased.entity.User;
import uz.pdp.apppermissionbased.entity.enums.RoleTypeEnum;
import uz.pdp.apppermissionbased.payload.SignDTO;
import uz.pdp.apppermissionbased.repository.RoleRepository;
import uz.pdp.apppermissionbased.repository.UserRepository;
import uz.pdp.apppermissionbased.service.AuthService;
import uz.pdp.apppermissionbased.utils.AppConstants;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(AuthController.BASE_PATH)
@RequiredArgsConstructor
public class AuthController {

    public static final String BASE_PATH = AppConstants.BASE_PATH + "/auth";
    public static final String SIGN_UP_PATH = "sign-up";
    public static final String SIGN_IN_PATH = "sign-in";
    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String sender;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;
    private final RoleRepository roleRepository;

    @PostMapping(SIGN_UP_PATH)
    public String signUp(@RequestBody @Valid SignDTO signDTO) {
        if (userRepository.existsByUsername(signDTO.getUsername()))
            return "User with" + signDTO.getUsername() + "\t already exists";

        User user = new User(
                signDTO.getUsername(),
                passwordEncoder.encode(signDTO.getPassword()),
                roleRepository.findByRoleType(RoleTypeEnum.USER).orElseThrow()
        );
        userRepository.save(user);
        CompletableFuture.runAsync(() -> sendVerificationCodeToEmail(user));   // bu
        return "Successfully registered";
    }

    @PostMapping(SIGN_IN_PATH)
    public String signIn(@RequestBody @Valid SignDTO signDTO) {
        return authService.signIn(signDTO);
    }


    private void sendVerificationCodeToEmail(User user) {
        SimpleMailMessage mailMessage
                = new SimpleMailMessage();

        // Setting up necessary details
        mailMessage.setFrom(sender);
        mailMessage.setTo(user.getUsername());
        mailMessage.setSubject("VERIFICATION PHASE");
        mailMessage.setText("CLICK_LINK" + "http://localhost/api/auth/verification-email/" + user.getUsername());
        // Sending the mail
        javaMailSender.send(mailMessage);
    }
}
