package uz.pdp.apppermissionbased.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uz.pdp.apppermissionbased.security.MyFilter;
import uz.pdp.apppermissionbased.utils.AppConstants;

@EnableWebSecurity   // Bu anotatsiy Spring ga barcha Security configurationlarni shu yerdan o'qishini ko'rsatish uchun ishlatamiz.
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        jsr250Enabled = true,
        securedEnabled = true)   // @EnableGlobalMethodSecurity  anotatsiyasi barcha methodlar uchun security ni yoqib qoyadi. Agar Security ga bog'liq anotatsiya qaysidir method ga qoyilsa demak ushbu Security class talablaridan (configuration) laridan o'tadi.
                                    // Uni ichidagi property lar prePostEnabled=true  bu  Spring Security ni pre/post anotatsiyalarini ruxsat etish uchun (enable)   Bu proyekta biz preAuth
public class SecurityConfig {

    // Security config classimizda MyFilter fieldi bor bu o'zimiz ochgan Filter.
    private final MyFilter myFilter;

    public SecurityConfig(@Lazy MyFilter myFilter) {
        this.myFilter = myFilter;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .csrf()
                .disable()
                .authorizeHttpRequests(f -> f
                        .requestMatchers(AppConstants.OPEN_PAGES)
                        .permitAll()
                        .requestMatchers("/*")
                        .permitAll()
                        .requestMatchers(AppConstants.BASE_PATH + "/**")
                        .authenticated())
                .addFilterBefore(myFilter, UsernamePasswordAuthenticationFilter.class)
                .build();

    }


    /**Password encoder ni bean qilibn olamiz user larni password larini encode qilib db da saqlash uchun vs tekshirayotganimizda decode qilamiz.
     * PasswordEncoder bu interface uni implementatsiya qilgan concrete classdan object qaytarvoramiz  (bir qancha concrete classlar bor biz BCryptPasswordEncoder ishlatdik .
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }


}
