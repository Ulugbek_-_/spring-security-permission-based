package uz.pdp.apppermissionbased.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import uz.pdp.apppermissionbased.entity.Category;
import uz.pdp.apppermissionbased.payload.CategoryDTO;
import uz.pdp.apppermissionbased.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public String addCategory(CategoryDTO categoryDTO){
        if(categoryDTO.getName().isEmpty())
            return "Category name is blank. Please fill the field";
        Category category = new Category();
        category.setName(categoryDTO.getName());
        categoryRepository.save(category);
        return "New category successfully added ✅";
    }

    public List<Category> categoryList (){
        List<Category> categoryList = categoryRepository.findAll();
        return categoryList;
    }

    public Category getCategory (UUID uuid){
        Optional<Category> optionalCategory = categoryRepository.findById(uuid);
        if(optionalCategory.isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Category with " + uuid + "  id does not exists");
        Category category = optionalCategory.get();
        return category;
    }


    public String deleteCategory(UUID id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if(optionalCategory.isEmpty())
            throw new RuntimeException("Category with " + id + "  id does not exists");
        Category category = optionalCategory.get();
        categoryRepository.delete(category);
        return "Successfully removed";
    }


    public String editCategory(CategoryDTO categoryDTO, UUID id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if(optionalCategory.isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Category with " + id + "  id does not exists");
        Category category = optionalCategory.get();
        category.setName(categoryDTO.getName());
        categoryRepository.save(category);
        return "Successfully updated";
    }
}
