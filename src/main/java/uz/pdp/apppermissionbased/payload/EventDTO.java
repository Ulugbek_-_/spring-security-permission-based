package uz.pdp.apppermissionbased.payload;

import lombok.Getter;

@Getter
public class EventDTO {

    private String name;

    private String description;

    private Integer maxTicketCount;

    private Integer hallId;



}
