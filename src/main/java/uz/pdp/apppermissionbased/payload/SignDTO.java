package uz.pdp.apppermissionbased.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;

@Getter
public class SignDTO {

    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
