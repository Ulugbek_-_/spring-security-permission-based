package uz.pdp.apppermissionbased.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.apppermissionbased.entity.User;

import java.util.Collection;

@RequiredArgsConstructor
public class UserPrincipal implements UserDetails {

    private final User user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRole().getPermissions();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }
}


/**
 * Bu class user haqidagi ma'lumotlarni olish uchun UserDetails inteface ni implemets qigan class. O'zimizni entity classimizga
 * ham qo'ysak bo'lardi lekin entity boshqa vazifalarni bajarmasin faqat entity jpa orqali databasedan olish va yozish
 * (o'zgartirish, o'chirish)  vazifalarini bajarsin faqat shuning uchub aloxida yana bitta class ochib uni ichida User classini
 *  fieldi va UserDetails interface ni bir qancha kerakli method larini override qilib oladi
 *
 *
 */