package uz.pdp.apppermissionbased.entity.template;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@MappedSuperclass
@Getter
public class AbsUUIDEntity {

    @Id
    @GeneratedValue(generator = "uuid_my_id")
    @GenericGenerator(name = "uuid_my_id", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
}


/** Bu class UUID type lik  id fieldli bo'gan classlarda qayta-qayta yozmaslik uchun bitta aloxida class ga chiqarib qo'ydik
 *  maqsad code ni qayta takrorlamaslik va reuse. BU classni extends qiladigan classlarda ham shu field lar bo'ladi lekin bu
 *  bu Spring ni configuration lari ishlatganda keyin sal boshqacharoq ishlaydi. Masaln javada bitta class 2 chisdan voris olsa
 *  shundoq field method lari o'tdigan bo'lsa bu yerda unday bo'lmaydi. Biroz qo'shimcha kiritish kerak aniqrog'i
 *  @MappedSuperclass anotatsiyasini ota class ga yozib qo'yamiz endi boshqa classlarga ham o'tadi  yo'q  o'tmaydi @Getter
 *  anotatsiyasini ham qo'yish lozim bola class bu fiel larni Get qilishi uchun.
 *
 *  UUID ni generatsiya qilish:
 *  ODDIY INTEGER NI GENERATSIYA QILISHNI KO"RGANIMIZ @GeneratedValue(strategy = GenerationType.IDENTITY)  ishlatamiz
 *  Lekin UUID da birinchi Generator yasab olamiz => @GenericGenerator(name = "uuid_my_id", strategy = "org.hibernate.id.UUIDGenerator")
 *  unga nom beramiz va strategy sini qo'yamiz undan keyin @GeneratedValue ni ichiga ushbu generatorimizni beramiz  =>
 *  @GeneratedValue(generator = "uuid_my_id")
 *
 */