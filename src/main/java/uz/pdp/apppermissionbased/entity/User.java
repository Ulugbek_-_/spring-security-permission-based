package uz.pdp.apppermissionbased.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Target;
import uz.pdp.apppermissionbased.entity.template.AbsUUIDEntity;

@Entity
@Table(name = "users")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbsUUIDEntity {

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @ManyToOne(optional = false)
    private Role role;
}

/**
 * class User   MANY TO ONE     Role       Many userlarda one Role    Userlar ko'p lekin ularda bitta role bor
 */
