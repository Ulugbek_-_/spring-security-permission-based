package uz.pdp.apppermissionbased.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import uz.pdp.apppermissionbased.entity.template.AbsUUIDEntity;

import java.util.UUID;

@Entity
@Getter
@Setter
public class Category extends AbsUUIDEntity {

    @Column(nullable = false)
    private String name;
}


    /**
     *  Category da id va name bor lekin id ni AbsUUIDEntity aloxida UUID si bor bitta classdan olayapti uni qayta qayta
     * classlar ichida yozmaslik uchun
     */

