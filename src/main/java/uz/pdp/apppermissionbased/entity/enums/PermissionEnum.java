package uz.pdp.apppermissionbased.entity.enums;

import org.springframework.security.core.GrantedAuthority;

public enum PermissionEnum implements GrantedAuthority {
    CATEGORY_ADD,
    CATEGORY_EDIT,
    CATEGORY_DELETE,
    CATEGORY_LIST,
    CATEGORY_BY_ID;

    @Override
    public String getAuthority() {
        return name();
    }
}

/**
 * Bu yerda barcha huquq (permission , authorities) larni enum qilib yozib chiqanmiz.
 *
 */