package uz.pdp.apppermissionbased.entity;

import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;
import uz.pdp.apppermissionbased.entity.enums.PermissionEnum;
import uz.pdp.apppermissionbased.entity.enums.RoleTypeEnum;

import java.util.List;
import java.util.Set;

@Entity
@Getter
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, unique = true)
    private RoleTypeEnum roleType;

    @ElementCollection
    @CollectionTable(
            uniqueConstraints = {
                    @UniqueConstraint(columnNames = {"role_id", "permissions"})})
    @Enumerated(EnumType.STRING)
    private Set<PermissionEnum> permissions;

    public Role(String name, RoleTypeEnum roleType, Set<PermissionEnum> permissions) {
        this.name = name;
        this.roleType = roleType;
        this.permissions = permissions;
    }
}

/**
 *   @Enumerated(EnumType.STRING) anotatsiyasi Set<PermissionEnum> permissions  qo'yilishini sababi collecctionimiz saqlaydigan
 *   class Type enum bogani uchun uni qo'ymasak ham ishlaydi lekin default holatda unday holatda db saqlavotganda enumni
 *   nomini emas (m-n: ADMIN, USER ...)  uni order (tartib raqami) ni olib db saqlab qoyadi ya'mi 1 ,2 shu kabi shuning
 *   uchun @Enumerated(EnumType.STRING)  anotatsiyasi ishlatiladi.
 */