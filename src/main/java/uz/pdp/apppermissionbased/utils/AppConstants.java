package uz.pdp.apppermissionbased.utils;

import uz.pdp.apppermissionbased.controller.AuthController;

/**
 * Bu yerda barcha constanta larni yozib qoyganmiz
 */

public interface AppConstants {

    String[] OPEN_PAGES = {
            AuthController.BASE_PATH + "/**",
    };   // OPEN_PAGES ga barcha ochiq yo'llar (URL) mizni yozib qo'yamiz array qilib. Hozirgi holatda arrayni ichida AuthController.BASE_PATH + "/**"  ya'ni AuthController
        // classini ichidagi BASE_PATH o'zgaruvch BASE_PATH = AppConstants.BASE_PATH + "/auth"  => /api/auth/**   yani  /api/auth/ dan keyingi barcha yo'llar  ochiq yo'llarga ochiq yo'llarga array ga qo'shib qo'yganmiz
        // Agar /api/auth/*  bitta yulduzcha bo'sa /api/auth dan keyin 1 ta yo'l degani ya'ni  /api/auth/sigIn degan bo'sa bunga ruxsat lekin /api/auth/sigIn/6   ko'rib turganimizdek /api/auth dan keyin 2 ta / (yo'l) bor bunday holatda  bitta * yulduzcha hato hattoki 1 ta yo'ldan keyin path variable kesa ham

    //AUTH_HEADER = "Authorization"  bu auth qilinayotganda header da keladigan so'zni ham aloxida o'zgaruvchiga olib qo'yganmiz  hozirda  "Authorization"  keyinchalik misol uchun "Bolta" deb qo'yadigan bo'sak ham shu variable ni o'zgartiramiz holos Dynamic tarzda qoganlariyam o'zgaratadi
    String AUTH_HEADER = "Authorization";

    // Bu Basic authentication uchun auth type.  Basic auth da user har bir requestida request headerida username va password yuboradi sign in qilmaydi har bir sorovda shu 2 ta narsani yuboradi. BIz uni backend tomonda request headeridan ovolamiz u bunday korinishda keladi  "Basic  tesha@gmail.com:root123"
    // Bu yerda biz kegan String ni ichini tekshiramiz Basic so'zi bilan boshlanganmi keyin uni ichidan  username:password ni  ikki nuqata (:) bilan ajratilgan bo'ladi uni ajratib olamiz String classini split() methodi orqali ajratib olib bitta arrayga saqlab qo'yamiz userData misol uchun arraydi 0 chi indexida  username va 1 chisida esa passwordi misol uchun
    String AUTH_TYPE_BASIC = "Basic ";

    //Bu  AUTH_TYPE_BEARER = "Bearer "  auth type ni yozib qoyishimiz uchun bunisi bearer uchun ( JWT  JSON WEB TOKENS )  Bearer so'zi bilan boshlanishi kerak
    String AUTH_TYPE_BEARER = "Bearer ";


    // Bu base path miz. Applicationimizdagi barcha yo'llar /api bilan boshlanadi deb belgilab qo'yishimiz uchun bitta o'zgaruvchiga olib qoyganmiz.Bundan maqsad ertaga base path mizni o'zgartiradigan bo'lsak barcha controllerni bittalab o'zgartirib chiqamasdan faqat shu o'zgaruvchini o'zgartiramiz holos (Hardcode dan qoshich)
    String BASE_PATH = "/api";
}
