package uz.pdp.apppermissionbased.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.apppermissionbased.entity.Role;
import uz.pdp.apppermissionbased.entity.User;
import uz.pdp.apppermissionbased.entity.enums.PermissionEnum;
import uz.pdp.apppermissionbased.entity.enums.RoleTypeEnum;
import uz.pdp.apppermissionbased.repository.RoleRepository;
import uz.pdp.apppermissionbased.repository.UserRepository;

import java.util.Objects;
import java.util.Set;

/** Data Loader classimiz bu class ni vazifasi application run boganda db application boshida bir marta load (yuklab olish) qilishi
 * kerak bo'lgan mae'lumotlarni yozamiz. Ya'ni application run qilganimizda birinchi bo'lib dataLoader classimiz ishlaydi
 * undan keyin main methodimiz ishlaydi. Data loader da application properties dan @Value anotatsiyasi orqali u yerdan ddl-auto
 * ni qiymatini olib boshida bir marta run boganda ddl-auto create boganda shu narsalarni qo'shib qo'y (yaratib qo'y)
 * yoki ishlatib qo'y deb yozib qo'yishimiz mumkin.  Uning uchun ushbu DataLoader classimiz CommandLineRunner interface ni
 * implements qilish kerak va unda bittagini run(String... args)  methodi bor ayni shu method barcha DataLoader vazifasini
 * bajaradi.  Bizni applicationimizda DataLoader ni ichida bitta ADMIN va USER degan role yasab db ga saqlamoqda va har bir
 * role ni ichiga huquqlarini (authorities) ham qo'shib ketmoqda. Barcha authorities aloxida bitta enum qilib qo'yilgan PermissionEnum degan enumda
 * Undan keyin 1 User ochib qo'shib qo'ymoqda db ga ADMIN user
 *
 */

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlMode;

    @Override
    public void run(String... args) throws Exception {
        if (Objects.equals(ddlMode, "create")) {
            Role admin = roleRepository.save(new Role(
                    "ADMIN",
                    RoleTypeEnum.ADMIN,
                    Set.of(PermissionEnum.values())
            ));

            roleRepository.save(new Role(
                    "USER",
                    RoleTypeEnum.USER,
                    Set.of(
                            PermissionEnum.CATEGORY_LIST,
                            PermissionEnum.CATEGORY_BY_ID)
            ));

            userRepository.save(new User(
                    "admin@admin.com",
                    passwordEncoder.encode("Root_123"),
                    admin
            ));
        }
    }
}
